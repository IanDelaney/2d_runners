if homedir()=="/users/idelaney"
    using Pkg
    Pkg.activate(".")
end

# ISM-like synthetic run
using VAWTools
using JLD2
using DifferentialEquations
using StatsBase
using Parameters
using SUGSET
using SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, source_water, width, source_till, thick, Valley, source_water_ddm, find_sp, year, day, tol_tester, Num

if homedir()== "/Users/Ian"
    fp_save = "/Users/Ian/research/SUGSET-2D/model_outputs/"
elseif homedir()=="/home/tesla-k20c"
    fp_save="/home/tesla-k20c/ssd/iand/outputs/"
elseif homedir()=="/users/idelaney"
    fp_save="/scratch/idelaney/outputs/"
end

grid_num =  [200, 150, 100, 50]
tol = [1e-10, 1e-9, 1e-8, 1e-7, 1e-6]


pp = Phys()

pg= Valley(tspan = (100*day,120*day), )
pg= Valley(pg,
           fsl=2,
           )


pn = Num()
Δσ = pn.Δσ

htic=5*Δσ
#ht0,runs=SUGSET.spinner(Valley,pg,pp,pn,1e-3,maxiter=100) 

mass_con_array, source_array, ΔV_array, cell_size, cell_number, time_array =  tol_tester(Valley, grid_num, tol, pg, pp, pn, htic)



@save "$(fp_save)tolerance_test_results.jld2" mass_con_array source_array ΔV_array time_array cell_size cell_number grid_num  tol Δσ
