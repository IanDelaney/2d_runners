using Parameters
using PyPlot
import Roots: find_zero
using StatsBase
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,startup

glacier = "gries";experiment="annual"; include("gries_annual.jl"); include("comparison_functions.jl")
#fp ="/Users/Ian/data/sed_modeling_data/gries/"
#glacier = "gorner";experiment="adsafasdnnual"; fp ="/Users/Ian/data/sed_modeling_data/gorner/"

if homedir()== "/Users/Ian"
    fp ="/Users/Ian/data/sed_modeling_data/$(glacier)/"
    fp_save = "/Users/Ian/research/SUGSET-2D/model_outputs/"
elseif homedir()=="/home/tesla-k20c"
    fp ="/home/tesla-k20c/iand/data/sed_modeling_data/$(glacier)/"
    fp_save="/home/tesla-k20c/iand/outputs/"
end

run_type = "single_run"
#run_type = "grid_run"

println("Testting... First 4 years")
data=data[1:3,:]

include("real_glacier_processing_2D.jl")

run_name="$(glacier)_test.jld"

pp=Phys(Dm=0.025,
        )

if glacier == "gries"
    pp=Phys(pp,
            Kg=1e-6,
            l_er=2.02
            )
end



odeopts = Num().odeopts # get defaults
odeopts[:abstol] =  odeopts[:reltol]= 1e-7

pn=Num(odeopts=odeopts,
       Δσ=1e-3)
pg= SUGSET.make_stack(pg.tout[1],RealGlacier, pg)
a=[]
inpol=zeros(pg.ncells)
for j = 1:pg.ns
    for i = 1:pg.nh
        ij = SUGSET.ij_return(i,j,pg.nh)
        inpol[ij]= SUGSET.is_inpoly(pg.sv[ij],pg.hv[ij],pg)
        if inpol[ij]==1.0
            push!(a,ij)
        end
        
    end
end

ht0=pg.ht0
ht0[a] .= pg.till_growth_lim
pg = RealGlacier(pg,
                 fsl=5.0,
                 source_average_time=2.5*day,
                 stack_reorder =true,
                 ht0=ht0
                 )    
if run_type == "single_run"
    err, rank_cor, abs_err, runs = comparerer_annual(data,pg,pp,pn)
elseif run_type == "grid_run"
    include("grid_search_pmap.jl")
    walltimes, vari_1, vari_2, vari_3,vari_4, model_abs_err, model_err_sum, model_rank_cor, runs =
        grid_search_annual(pp, pg, pn, data, runoff_date_spin, runoff_date_mod, fp_save)
    
end

error()
