using PyPlot
using VAWTools
using JLD2
using StatsBase
ENV["PYTHON"]=""
using Parameters

if homedir()== "/Users/Ian"
    fp_load = "/Users/Ian/research/SUGSET-2D/model_outputs/"
    fp_save = "/Users/Ian/research/SUGSET-2D/figs/"
end



@load "$(fp_load)tolerance_test_results.jld2" mass_con_array source_array ΔV_array time_array cell_size cell_number grid_num tol #Δσ



tol_colors =["red"
             "darkorange"
             "green"
             "steelblue"
             "orchid"
             "blueviolet"
             "silverb"
             ]

cell_colors = ["tomato"
               "peru"
               "dodgerblue"
               "purple"
               "indigo"
               "turquoise"
               "peru"
               ]


############
### plot ###
############
cell_size=cell_size[:,1].^.5
figure(figsize=(10,25))
fig, axs = subplots(3,2,figsize=(10,25))



sca(axs[1])
# err vs tol

#semilogy(cell_size, mean(abs.(mass_con_array),dims=2) ,linewidth="2.5",color="black", marker="o",label = "mean" );
for i = 1:length(tol)
    #plot(cell_size, abs.(mass_con_array[:,i]) ,linewidth="1", color=cell_colors[i], marker="o",label = "tol: $(tol[i]) m" );
    semilogy(cell_size, abs.(mass_con_array[:,i]) ,linewidth="1", color=cell_colors[i], marker="o",label = "tol: $(tol[i]) m" );
   
    ylabel("Qₛ÷ ΔV (-)",fontsize=14)
 #   xlabel("cell edge length (m)", fontsize=14)    
    ylim([1e-5 , 1.5e1])
end
text(30,5, "a",fontsize=14)
#text(70,0.012, "Qₛ÷ ΔV = 0.01",fontsize=14)
axhline(y=0.01, color="black")
legend(loc=0, ncol=2,fontsize="small")

# err vs grid-size
sca(axs[4])
#semilogy(tol, mean(abs.(mass_con_array),dims=1)' ,linewidth="2.5",color="black", marker="o",label = "mean" )
for i = 1:length(grid_num)
    #plot(tol, abs.(mass_con_array[i,:]) ,linewidth="1", marker="o", color=tol_colors[i],label = "cell edge length: $(Int(round(cell_size[i],digits=0))) m" )
    loglog(tol, abs.(mass_con_array[i,:]) ,linewidth="1", marker="o",color=tol_colors[i], label = "cell size: $(Int(round(cell_size[i],digits=0))) m" );

    #ylabel("mass conservation (-)",fontsize=14)
   # xlabel("tolerance (m)", fontsize=14)
    ylim([1e-5 , 1.5e1])
end
text(1e-10,5, "b",fontsize=14)
text(1e-10,0.012, "Qₛ÷ ΔV = 0.01")
axhline(y=0.01, color="black")

legend(loc=0, ncol=2,fontsize="small")


sca(axs[2])
#plot(cell_size, mean(abs.(ΔV_array),dims=2) ,linewidth="2.5", color="black", marker="o",label = "mean" );    
for i = 1:length(tol)
    plot(cell_size, abs.(ΔV_array[:,i]) ,linewidth="1", marker="o", color=cell_colors[i],label = "tol:  $(tol[i]) m" );
    #plot(cell_size, abs.(ΔV_array[:,i]) ,linewidth="1", marker="o",label = "tol:  $(tol[i]) m" );    
    ylabel("Δ Volume (m³)",fontsize=14)
  #  xlabel("cell edge length (m)", fontsize=14)    
    ylim([ minimum(minimum(abs.(ΔV_array)))*.95 , maximum(maximum(abs.(ΔV_array)))*1.05])
end
text(30,maximum(abs.(ΔV_array)), "c",fontsize=14)


sca(axs[5])
#plot(tol, mean(abs.(ΔV_array),dims=1)' ,linewidth="2.5", color="black",  marker="o",label = "mean" )
for i = 1:length(grid_num)
    semilogx(tol, abs.(ΔV_array[i,:])' ,linewidth="1", marker="o",color=tol_colors[i],label = "cell edge length: $(Int(round(cell_size[1],digits=0))) m" )
    #plot(tol, abs.(ΔV_array[i,:])' ,linewidth="1", marker="o", label = "cell size: $(Int(round(cell_size[1],digits=0))) m²" )
    #ylabel("mass conservation (-)",fontsize=14)
   # xlabel("tolerance (m)", fontsize=14)
    ylim([ minimum(minimum(abs.(ΔV_array)))*.95 , maximum(maximum(abs.(ΔV_array)))*1.05])
end
text(1e-10,maximum(abs.(ΔV_array)), "d",fontsize=14)
#savefig("$(fp_save)mass_con_ΔV_check.pdf")

#figb, axsb = subplots(1,2,figsize=(10,6))


sca(axs[3])
#semilogx(cell_size,mean(abs.(time_array./60),dims=2) ,linewidth="2.5",color="black", marker="o",label = "mean" );    
for i = 1:length(tol)
    #semilogx(cell_size, time_array[:,i]./60 ,linewidth="1",color=colors[i], marker="o",label = "tol: $(tol[i]) m" );    
    #plot(cell_size, time_array[:,i]./60 ,linewidth="1", marker="o",label = "tol: $(tol[i]) m" );
    semilogy(cell_size, time_array[:,i]./maximum(time_array) , color=cell_colors[i],linewidth="1", marker="o",label = "tol: $(tol[i]) m" );

    xlabel("cell edge length (m)", fontsize=14)

end
ylim([5e-5, 5])
text(30,1.20, "e",fontsize=14)
ylabel("normalized wall-time", fontsize=14)
xlim([minimum(minimum(abs.(cell_size)))*.9  , maximum(maximum(abs.(cell_size)))*1.05])

# text(100, 1.1,"minute")
# axhline(1,color="black")
# text(100, 60.1,"hour")
# axhline(60,color="black")


#legend()

sca(axs[6])
#semilogx(tol, mean(abs.(time_array./60),dims=1)' ,linewidth="2.5", color="black",  marker="o",label = "mean" )
#ylim([minimum(minimum(abs.(time_array./60)))*.9 , maximum(maximum(abs.(time_array./60)))*1.05])
for i =1:length(grid_num)
    #plot
    loglog(tol,time_array[i,:]./maximum(time_array),linewidth="1", marker="o",color=tol_colors[i], label = "cell edge length: $(Int(round(cell_size[i],digits=0))) m" )

    #plot(tol,time_array[i,:]./60,linewidth="1", marker="o",color=tol_colors[i], label = "cell edge length: $(Int(round(cell_size[i],digits=0))) m" )
    #ylabel("walltime for 30 days (minutes, model time)", fontsize=14)    
    #semilogx(tol,time_array[i,:]./60,linewidth="1", marker="o", label = "cell size: $(Int(round(cell_size[i],digits=0))) m²" )
    xlabel("tolerance (m)", fontsize=14)

end
ylim([5e-5, 5])
text(1e-10,1.2, "f",fontsize=14)
#legend()
#xlim([minimum(minimum(abs.(tol)))*.9  , maximum(maximum(abs.(tol)))*1.05])
# text(1.1e-7, 1.1,"minute")
# axhline(1,color="black")
# text(1.1e-7, 60.1,"hour")
# axhline(60,color="black")
#tight_layout()

suptitle("Model performance with grid size and solving tolerance", fontsize="x-large")
savefig("$(fp_save)performance.pdf")
