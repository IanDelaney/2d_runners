#topography and hydrology input
using DelimitedFiles, Dates
using Interpolations
using Dierckx, ImageFiltering
using VAWTools
using OffsetArrays
using Statistics
using JLD2
using WhereTheWaterFlows
include("runoff_sort.jl")

#############
### Gries ###
#############
b_0_read_in = readdlm("$(fp)gries_2d_b_0_2009_2016.dat",',',String) # read in runoff
b_0_spin_read_in = readdlm("$(fp)gries_2d_b_0_2009_2016.dat",',',String) # read in runoff

bed_dem=read_agr("$(fp)$(glacier)_topo/dem_bed_gries.agr",Float64)
surf_dem = read_agr("$(fp)$(glacier)_topo/gries_dem_2012_25m.agr",Float64)
outline,split_poly=VAWTools.concat_poly(read_xyn("$(fp)$(glacier)_topo/rand_20120827_subset.xyzn",hasz=true,fix=false))

##########################
### Process topography ###
##########################
inpoly_surf=Base.trues(length(surf_dem.x),length(surf_dem.y)) ### might need to sort out x,y,z... as for gries.
inpoly_bed=Base.trues(length(bed_dem.x),length(bed_dem.y)) ### might need to sort out x,y,z... as for gries.

for i=1:size(inpoly_surf,1)
    for j=1:size(inpoly_surf,2)
        pt= [surf_dem.x[i],surf_dem.y[j]]
        inpoly_surf[i,j]=inpoly(pt,outline[1:2,:])
    end
end

for i=1:size(inpoly_bed,1)
    for j=1:size(inpoly_bed,2)
        pt= [bed_dem.x[i],bed_dem.y[j]]
        inpoly_bed[i,j]=inpoly(pt,outline[1:2,:])
    end
end

surf_x = repeat(surf_dem.x,1,length(surf_dem.y))
surf_y = repeat(surf_dem.y,1,length(surf_dem.x))'
surf_z=   Array(surf_dem.v)

bed_x = repeat(bed_dem.x,1,length(bed_dem.y))
bed_y = repeat(bed_dem.y,1,length(bed_dem.x))'
bed_z= Array(bed_dem.v)


########## Using Dierckx ... has derivative functions...
nodes =(convert(Array,surf_dem.x[:]),convert(Array,surf_dem.y[:]));
_,_,dir, _, _,pits, _, _ = waterflows(surf_z)
stuff_b =  WhereTheWaterFlows.fill_dem(surf_z, pits, dir) 
spl_surf =  extrapolate(interpolate(nodes, mapwindow(median!,stuff_b,(15,15)), Interpolations.Gridded(Linear())),Flat())


nodes =(convert(Array,bed_dem.x[:]),convert(Array,bed_dem.y[:]));
stuff_c= mapwindow(median!,bed_z,(15,15) )
_,_, dir, _, _, pits, _, _ = waterflows(stuff_c)
stuff_b =  WhereTheWaterFlows.fill_dem(stuff_c, pits, dir)


spl_bed_ = extrapolate(interpolate( stuff_b, BSpline(Quadratic(Line(OnGrid())))),Flat())
spl_bed  = Interpolations.scale(spl_bed_, bed_dem.x[:], bed_dem.y[:])

thickness = max.(0.0, spl_surf(nodes[1],nodes[2]).-spl_bed(nodes[1],nodes[2]))

spl_inpoly= extrapolate(interpolate(nodes, inpoly_bed, Interpolations.Gridded(Constant())),Flat())

###########################
### process runoff data ###
###########################


b_0, runoff_date_mod   = runoff_b0_sort!(b_0_read_in)
b_0_spin, runoff_date_spin = runoff_b0_sort!(b_0_spin_read_in)

runoff_date_mod=runoff_date_mod .+90*day
runoff_date_mod_spin=runoff_date_spin .+90*day

include("interface-real-glacier_2d.jl")

pp = Phys(fi=5,
          ft=5)
pn = Num()
pg = RealGlacier(name=glacier,
                 tspan = (runoff_date_mod[1],runoff_date_mod[end]),
                 tspan_spin = (runoff_date_mod[1],runoff_date_mod[1]+year),
                 tspan_run =(runoff_date_mod[1],runoff_date_mod[end]),
                 spl_surf = spl_surf,
                 spl_bed = spl_bed,
                 is_in_poly = spl_inpoly
                 )

pg,pp,pn = startup(pg, RealGlacier, pp,pn)

pg = RealGlacier(pg,
                 ht0=  zeros(pg.ncells),
                 zs_min= 2500)

function run_n_save(pg,pp,pn,thresh,run_name,fp_save)
    
    println("FSL=$(pg.fsl)")
    println("beginning spin")
    
    ht0,runs=SUGSET.spinner(RealGlacier,pg,pp,pn,thresh;verbose=true) 
    pg = RealGlacier(pg,
                 ht0=ht0
                 )    
    
    println("end spin")
    odeopts=Num().odeopts
    odeopts[:saveat]= pg.tout
    odeopts[:save_everystep ]= false
    pn =Num(pn,
            odeopts=odeopts)
    
    println("starting run_model()")
    println(Dates.now())
    @time sol  = run_model(pg, pp, pn)
    println("run_model() finished")
    
    @save "$(fp_save)$(run_name)_fsl$(pg.fsl)_sol.jld" sol pn pg pp runs ht0
    
    hts, dht_dts,sed_source, Qws, Qws_sm, Qbs, Qbes, Qbs_catch, Qws_catch = SUGSET.save_run(sol, pg, pp, pn, run_name,fp_save;verbose=false)

    return nothing
end




