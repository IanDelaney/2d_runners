using Parameters
using PyPlot
import Roots: find_zero
using StatsBase
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year

glacier = "gries";experiment="annual";fp ="/Users/Ian/data/sed_modeling_data/gries/"
# glacier = "gorner";experiment="anndafal"; fp ="/Users/Ian/data/sed_modeling_data/gorner/"

include("gries_processing.jl")

pp=Phys(pp,
        Dm=0.005,
        DT=15.0)

pn=Num(pn)

pg= SUGSET.make_stack(pg.tout[1],RealGlacier,pg)
               
shreve=zeros(pg.ncells)

t=1.0
for j = 1:pg.ns
    for i = 1:pg.nh
        ij = SUGSET.ij_return(i,j,pg.nh)
        shreve[ij]= pg.float_fracˣ.*SUGSET.thick(pg.sv[ij],pg.hv[ij],t,pg)*rw*g + rw*g*zb(pg.sv[ij],pg.hv[ij],t,pg)
    end
end

# pg=RealGlacier(pg,
#                ϕ=shreve
#                )



#ϕˣ=SUGSET.ϕ_return(Ψˣ_out,t,pg)

function printstff(ϕˣ,shreve,pg,pp,pn)
    for i = 1:24
        Qws,_,_=SUGSET.Qw_return(pg.tout[i],pg,pp,pn)
        edges= SUGSET.domain_edge(pg)
        Qw_sum = round(sum(Qws[edges]),digits=0)
        
        println("t: $(round(pg.tout[i]./day,digits=1)) Qw_sum: $(-Qw_sum) ϕˣ: $(round(mean(shreve),digits=2)) ϕ: $(round(mean(pg.ϕ),digits=1)) ff: $(round(mean(pg.ϕ)./mean(shreve),digits=2))")
        
    end
end


function makeani(pg,pp,pn,shreve) 
    figure(figsize=(18, 3))
    
    for i =1:50 

        Qws=SUGSET.Qw_return(pg.tout[i],pg,pp,pn)[1]
        #S=SUGSET.Dh2S.(Dhs, pp.hookeangle)
        #Ψ_ = SUGSET.Ψ.(Dhs,Qws,Ref(pp))
        println(maximum(-Qws))
        scatter(pg.sv[:], pg.hv[:],c=-Qws,cmap="GnBu");colorbar()
        clim([0 , 1])
        
        edges= SUGSET.domain_edge(pg)
        Qw_sum = round(sum(Qws[edges]),digits=0)
        title("Water discharge $(-Qw_sum) (m³ per s), Time: $(round(pg.tout[i]/day,digits=2))")
        pause(0.5)
        # subplot(1,2,2) 
        # scatter(sv[:], hv[:],c=-Qws,cmap="gist_earth");colorbar()
        
        # savefig("hydro_routing$i.png")
        clf()
        
     end
end

