using PyPlot
using VAWTools
using JLD2
using Images
using Distributions
import SUGSET: day,year
using StatsBase

ENV["PYTHON"]=""
using Parameters


if homedir()== "/Users/Ian"
    fp_load =  "/Users/Ian/research/SUGSET-2D/model_outputs/"
    fp_save =  "/Users/Ian/research/SUGSET-2D/figs/"
end

#################
### load data ###
#################

run_name="gries_test_best"

@load "$(fp_load)$(run_name).jld2" pg pp pn hts dht_dts sed_source Qws Qws_sm Qws_src Qbs Qbes zs_ zb_

tout=pg.tout
edges=pg.edges
nh=pg.nh
ns=pg.ns
sv=pg.sv
hv=pg.hv

# Qb_year_mean  = mean(reshape(sum(-Qbs[edges,1:end-1],dims=1)',1460,:),dims=2)
# Qbes_year_mean  = mean(reshape(sum(-Qbes[edges,1:end-1],dims=1)',1460,:),dims=2)
# Qw_year_mean = mean(reshape(sum(-Qws[edges,1:end-1],dims=1)',1460,:),dims=2)
# dhdt_year_mean = mean(reshape(mean(dht_dts[edges,1:end-1],dims=1)',1460,:),dims=2)

# Qb_no_season = (reshape(sum(-Qbs[edges,1:end-1],dims=1)',1460,:) .- Qb_year_mean)[:]
# Qbes_no_season = (reshape(sum(-Qbes[edges,1:end-1],dims=1)',1460,:) .- Qbes_year_mean)[:]
# Qw_no_season = (reshape(sum(-Qws[edges,1:end-1],dims=1)',1460,:) .- Qw_year_mean)[:]
# dhdt_no_season= (reshape(mean(dht_dts[edges,1:end-1],dims=1)',1460,:) .- dhdt_year_mean)[:]

# Qb_year= zeros(Int(round(length(tout)/1460)))
# Qbes_year= zeros(Int(round(length(tout)/1460)))
# Qw_year= zeros(Int(round(length(tout)/1460)))
# dhdt_year= zeros(Int(round(length(tout)/1460)))

# Qb_no_season_year= zeros(Int(round(length(tout)/1460)))
# Qbes_no_season_year= zeros(Int(round(length(tout)/1460)))
# Qw_no_season_year= zeros(Int(round(length(tout)/1460)))
# dhdt_no_season_year = zeros(Int(round(length(tout)/1460))) 

# for i = 1:Int(floor(length(tout)/1460))
#     #    println(i)
#     if i== 1
#         inda= i
#     else
#         inda= (i-1)*1460
#     end
#     indb=Int(inda+1460)
#     Qb_year[i] = (tout[2]-tout[1]) * sum(-Qbs[edges,inda:indb])
#     Qbes_year[i] = (tout[2]-tout[1]) * sum(-Qbes[edges,inda:indb])
#     Qw_year[i] = (tout[2]-tout[1]) * sum(-Qws[edges,inda:indb])
#     dhdt_year[i] = (tout[2]-tout[1]) * mean(dht_dts[edges,inda:indb])
    
#     Qb_no_season_year[i] = (tout[2]-tout[1]) * sum(Qb_no_season[inda:indb])
#     Qbes_no_season_year[i] = (tout[2]-tout[1]) * sum(Qbes_no_season[inda:indb])
#     Qw_no_season_year[i] = (tout[2]-tout[1]) * sum(Qw_no_season[inda:indb])
#     dhdt_no_season_year[i] = (tout[2]-tout[1]) * sum(dhdt_no_season[inda:indb]) 

# end




######################
### start plotting ###
######################

fs=12

fig1 = figure(figsize=(12,5))

#################################
ax1 = plt.subplot2grid((2,2), (0,0), colspan=2)
if run_name == "alpine_30years_tgl_var"
    axvspan(12, 20,color="lightgray")
end

if run_name == "alpine_30years_tgl_var"
    title("Model with 30 years of forcing", fontsize=fs)
    axvspan(12, 20,color="lightgray")
    p1=plot(tout[1:4:end]./year, sum(-Qws[edges,1:4:end],dims=1)' ,linewidth="2", color="steelblue", label = "Water discharge" )
    xlim([7,25])
    text(7.5,17.5, "a",fontsize=fs*1.5)
    ylim([0,maximum(sum(-Qws[edges,1:4:end],dims=1)')])
else
    p1=plot(tout[1:4:end]./year .+ 1970, sum(-Qws[edges,1:4:end],dims=1)' ,linewidth="2", color="steelblue", label = "Water discharge" )
    text(2010.25,6.5, "a",fontsize=fs*1.5)
    xlim([2010, tout[end]./year .+ 1970])
    ylim([0.01, 8])
end

ylabel(L"Q_{w}~(m³ s⁻¹)",color="steelblue", fontsize=fs)
xticks([])

ax1b = twinx()
if run_name == "alpine_30years_tgl_var"
    p2=semilogy(tout./year, mean(hts,dims=1)',linewidth="2", color="darkorange", label = "Till height")
    yticks([0.021, 0.022, 0.024])
    ylabel("mean H (m)",color="darkorange", fontsize=fs)
    text(2009.5,.23, "a",fontsize=fs*1.5)
else
    plot(tout[1:4:end]./year .+ 1970 ,sum(-Qbs[edges,1:4:end],dims=1)',linewidth="2", color="chocolate", label ="Sediment discharge" )
    
    ylim([0.0000001, .7*maximum(sum(-Qbs[edges,1:4:end],dims=1)')])
    ylabel(L" Q_{s}~(m³ s⁻¹)",color="chocolate", fontsize=fs, rotation = 270, labelpad = 20)
  
end



xticks([])

subplots_adjust(hspace=0.0)
###############################################
ax2 = plt.subplot2grid((2,2), (1,0), colspan=2)
###############################################

if run_name == "alpine_30years_tgl_var"
    axvspan(12, 20,color="lightgray")
    plot(tout[1:4:end]./year,sum(-Qbs[edges,1:4:end],dims=1)',linewidth="2", color="chocolate", label ="Sediment discharge" )
    xlim([7,25])
    xlabel("Year", fontsize=fs)
    ylim([0.00001, 0.014])
    ylabel(L" Q_{s}~(m³ s⁻¹)",color="chocolate", fontsize=fs)

else
    p2=plot(tout./year .+1970 , mean(hts,dims=1)',linewidth="2", color="darkorange", label = "Till height")
    text(2009,3.3e-2, "a",fontsize=fs*1.5)
    xlabel("Year", fontsize=fs)
    #ylim([0.03767,0.03781])
    #yticks([0.03768, 0.0375, 0.0380])
    xlim([2010, tout[end]./year .+ 1970])
    ylabel("mean H (m)",color="darkorange", fontsize=fs)  
end

twinx()

if run_name == "alpine_30years_tgl_var"

    semilogy(tout[730:1460:end-730]./year, Qb_year ,linewidth="2", color="forestgreen", label = " Sediment discharge capacity " )
    #ylim([1e5, 7e5])
    xlabel("Year", fontsize=fs)
    ylabel(L"Q_{s} ~ (m³ a⁻¹)",color="forestgreen", fontsize=fs,rotation=270, labelpad = 2)
    text(7.5,7e3, "b",fontsize=fs*1.5)

else
    plot(tout[1:4:end]./year .+ 1970, sum(-Qbes[edges,1:4:end],dims=1)' ,linewidth="2", color="forestgreen", label = " Sediment discharge capacity " )
    xlabel("Year", fontsize=fs)
    ylim([0.00001, .6*maximum(sum(-Qbes[edges,1:4:end],dims=1)')])
    ylabel(L"Q_{sc} ~ (m³ s⁻¹)",color="forestgreen", fontsize=fs,rotation=270, labelpad = 20)
    text(2010.25,0.04, "b",fontsize=fs*1.5)
end
xlabel("Year", fontsize=fs)

if run_name == "alpine_30years_tgl_var"
    savefig("$(fp_save)alpine_ts.pdf")
else
    savefig("$(fp_save)gries_ts.pdf")
end


#####################################################################################
fig, axs = subplots(2,2,figsize=(12,5))
fs=11

if run_name == "alpine_30years_tgl_var"

    ax1=sca(axs[1])
    #subplot(2,2,1)
    im1=imshow(reshape(-Qbs[:,20075], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im1,shrink=.25,pad=.05, aspect=10)
    #ax3.set_label("Sediment discharge (m³ s⁻¹)")
    ylabel("y (m)");xlabel("x (m)")
    title("Sediment discharge (m³ s⁻¹) at year $(round(tout[20072]/year,digits=2))", fontsize=fs)

    ax2=sca(axs[2])
    #subplot(2,2,2)
    im2=imshow(reshape(-Qws[:,20075], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im2,shrink=.25,pad=.05,aspect=10)
    #ax3.set_label("Sediment discharge (m³ s⁻¹)")
    ylabel("y (m)");xlabel("x (m)")
    plt.title("Water discharge (m³ s⁻¹) at year $(round(tout[20072]/year,digits=2))", fontsize=fs)

    ax3=sca(axs[3])
    #subplot(2,2,3)
    im3=imshow(reshape(hts[:,1460*10], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im3,shrink=.25,pad=.05,aspect=10);clim([0,.11])#clim([0, maximum(-Qbs[:,20072+3650*3])])
    ylabel("y (m)");xlabel("x (m)")
    title("Till-layer height (m) at year $(round(tout[1460*10]/year,digits=2))", fontsize=fs)

    ax4=sca(axs[4])
    #subplot(2,2,4)
    im4=imshow(reshape(hts[:,1460*29], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im4,shrink=.25,pad=.05,aspect=10);clim([0,.11])
    title("Till-layer height (m) at year $(round(tout[1460*29]/year,digits=2))", fontsize=fs)
    ylabel("y (m)");xlabel("x (m)")
    subplots_adjust(hspace=-0.6)
    tight_layout()

    savefig("$(fp_save)alpine_spatial.pdf")
else

    fig, axs = subplots(2,2,figsize=(9,7))
    glacier_pts =[]
    for i = 1:length(Qws[:,400])
        if Qws[i,400] ==0.0
            push!(glacier_pts,i)
        end
    end

    index_pt= 9066
    index_pt= 9055
    
    ax1=sca(axs[1])
    #subplot(2,2,1)
    Qbs[glacier_pts,:] .= NaN
    Qws[glacier_pts,:] .= NaN
    hts[glacier_pts,:] .= NaN
    
    im1=imshow(reshape(-Qbs[:,index_pt], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im1,shrink=.25,pad=.05, aspect=10);clim([0, .00052])
    #ax3.set_label("Sediment discharge (m³ s⁻¹)")
    xticks(rotation=30)
    title("Sediment discharge (m³ s⁻¹) at year $(round(tout[index_pt]/year + 1970,digits=2))", fontsize=fs)
    ylabel("y (m)");xlabel("x (m)")
    
    ax2=sca(axs[2])
    #subplot(2,2,2)
    im2=imshow(reshape(-Qws[:,index_pt], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im2,shrink=.25,pad=.05,aspect=10);clim([0, 2])
    #ax3.set_label("Sediment discharge (m³ s⁻¹)")
    xticks(rotation=30)
    plt.title("Water discharge (m³ s⁻¹) at year $(round(tout[index_pt]/year + 1970,digits=2))", fontsize=fs)
    ylabel("y (m)");xlabel("x (m)")
    
    ax3=sca(axs[3])
    #subplot(2,2,3)
    im3=imshow(reshape(hts[:,1595], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im3,shrink=.25,pad=.05,aspect=10);clim([1e-4,0.012])
    xticks(rotation=30)
    title("Till-layer height (m) at year $(round(tout[1595]/year + 1970,digits=2))", fontsize=fs)
    ylabel("y (m)");xlabel("x (m)")
    
    ax4=sca(axs[4])
    #subplot(2,2,4)
    im4=imshow(reshape(hts[:,index_pt], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im4,shrink=.25,pad=.05,aspect=10);clim([1e-4,0.012])
    xticks(rotation=30)
    title("Till-layer height (m) at year $(round(tout[index_pt]/year + 1970,digits=2))", fontsize=fs)
    ylabel("y (m)");xlabel("x (m)")
    
    subplots_adjust(hspace=0.2)
    tight_layout()
    

    savefig("$(fp_save)gries_spatial_alt.pdf")
end


error()
