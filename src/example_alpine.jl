using Pkg

Pkg.activate(".")

using Parameters
using Random
using JLD2
using DifferentialEquations,OrdinaryDiffEq
import Roots: find_zero
using StatsBase
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET:  zs,zb, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,Num,startup

if homedir()== "/Users/Ian"
    fp_save = "/Users/Ian/research/SUGSET-2D/model_outputs/"
elseif homedir()=="/home/tesla-k20c"
    fp_save="/home/tesla-k20c/ssd/iand/outputs/"
elseif homedir()=="/users/idelaney"
    fp_save="/scratch/idelaney/outputs/"
end

run_type = ["normal" "homogen_erosion" "seasonal_erosion"][1]

if run_type== "normal"
    run_name="alpine_30years_tgl_novar_DT5"
    #pg= Valley(tspan = (0,30*year),
    pg= Valley(tspan = (0,30*year),
               tspan_spin = (0, year),
              
               ) 
elseif run_type== "homogen_erosion" 
    run_name="alpine_10years_tgl_novar_homogen_erosion_DT5"
    pg= Valley(tspan = (0,10*year),
               tspan_spin = (0, year),
              
               )
elseif run_type== "seasonal_erosion" 
    run_name="alpine_10years_tgl_novar_seasonal_erosion_DT5"
    pg= Valley(tspan = (0,10*year),
               tspan_spin = (0, year),              
               )
end

include("interface_alpine.jl")

pp=Phys(Dm=0.01,ΔT=5)

pg=Valley(pg,
          tout= pg.tspan[1]:.25*day:pg.tspan[2],
          ht0= pg.till_growth_lim .+ zeros(pg.ncells))


pn=Num(Δσ=1e-3)

pg,pp,pn = startup(pg, Valley, pp,pn)

println("spin started()")
ht0,runs=SUGSET.spinner(Valley,pg,pp,pn,1e-4,maxiter=5) 
pg =Valley(pg,
           ht0=ht0
           )

odeopts=Num().odeopts
odeopts[:saveat]= pg.tout
odeopts[:save_everystep ]= false
pn =Num(pn,
        odeopts=odeopts)

println("starting run_model()")
println(Dates.now())
@time sol  = run_model(pg, pp, pn)
println("
    run_model() finished
    took $(runs) of years for spin            ")

@save "$(fp_save)$(run_name)_sol.jld2" sol pn pg pp runs


hts, dht_dts,sed_source, Qws, Qws_sm, Qbs, Qbes = SUGSET.save_run(sol, pg, pp, pn, run_name,fp_save;verbose=false)
exit()


