using Parameters
using PyPlot
import Roots: find_zero
using StatsBase
using Dates

using SUGSET
#using SUGSET: erosion, post_proc
import SUGSET:  zs,zb, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,Num,startup

run_type = ["normal" "homogen_erosion" "seasonal_erosion"][1]
pn=Num()
pg=Valley(tspan=(0, 30*365*86400))
pg=Valley(pg,
          tout= pg.tspan[1]:10*day:pg.tspan[2]
          )

pp=Phys()

pg,pp,pn = startup(pg, Valley, pp,pn)

include("interface_alpine.jl")
#ϕˣ=SUGSET.ϕ_return(Ψˣ_out,t,pg)

function printstff(pg,pp,pn)
    Qw_out =[]
    for i in eachindex(pg.tout)
        
        _,Qws,_,pg=SUGSET.Qw_return(pg.tout[i],pg,pp,pn)
        edges= SUGSET.domain_edge(pg)
        Qw_sum = round(sum(Qws[edges]),digits=0)
        push!(Qw_out,Qw_sum)
        
        if rem(pg.tout[i]/day,100) ==0
            println("time: $(pg.tout[i]/year)... Qw: $(Qw_sum)")
        end

    end
    return Qw_out
end

function makeani(pg,pp,pn,shreve) 
    figure(figsize=(18, 3))
    
    for i =1:50 

        Qws,Dhs,pg=SUGSET.Qw_return(pg.tout[i],pg,pp,pn)
        S=SUGSET.Dh2S.(Dhs, pp.hookeangle)
        Ψ_ = SUGSET.Ψ.(Dhs,Qws,Ref(pp))
        println(maximum(-Qws./S))
        scatter(sv[:], hv[:],c=pg.ϕ,cmap="GnBu");colorbar()
        clim([0 , 5e6 ]);
        
        edges= SUGSET.domain_edge(pg)
        Qw_sum = round(sum(Qws[edges]),digits=0)
        title("Water discharge $(-Qw_sum) (m³ per s), Time: $(round(pg.tout[i]/day,digits=2)) ff= $(mean(pg.ϕ/shreve))")
        pause(0.5)
        # subplot(1,2,2) 
        # scatter(sv[:], hv[:],c=-Qws,cmap="gist_earth");colorbar()
        
        # savefig("hydro_routing$i.png")
        clf()
        
     end
end


#run(`magick -delay $(interval/10) -loop 0 test5_\*.png test5.gif`)
        
