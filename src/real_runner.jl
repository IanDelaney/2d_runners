using Pkg
Pkg.activate(".")
using Parameters
using Random, Distributions
import Roots: find_zero
using StatsBase
using JLD2
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,startup


glacier = "gries";experiment="annual"; include("gries_annual.jl"); include("comparison_functions.jl")
#fp ="/Users/Ian/data/sed_modeling_data/gries/"
#glacier = "gorner";experiment="adsafasdnnual"; fp ="/Users/Ian/data/sed_modeling_data/gorner/"

if homedir()== "/Users/Ian"
    fp ="/Users/Ian/data/sed_modeling_data/$(glacier)/"
    fp_save = "/Users/Ian/research/SUGSET-2D/model_outputs/"
elseif homedir()=="/home/tesla-k20c"
    fp ="/home/tesla-k20c/ssd/iand/data/sed_modeling_data/$(glacier)/"
    fp_save="/home/tesla-k20c/ssd/iand/outputs/"
elseif homedir()=="/users/idelaney"
    fp ="/scratch/idelaney/data/$(glacier)/"
    fp_save="/scratch/idelaney/outputs/$(glacier)/"
end

run_type =["single" "parameter_search"][1]
include("gries_processing.jl")


pg= SUGSET.make_stack(pg.tout[1],RealGlacier, pg)
a=[]
inpol=zeros(pg.ncells)
for j = 1:pg.ns
    for i = 1:pg.nh
        ij = SUGSET.ij_return(i,j,pg.nh)
        inpol[ij]= SUGSET.is_inpoly(pg.sv[ij],pg.hv[ij],pg)
        if inpol[ij]==1.0
            push!(a,ij)
        end
    end
end

ht0=pg.ht0
ht0[a] .= 1e-3
pg = RealGlacier(pg,
                 inpoly=Int.(a),
                 tout= pg.tspan[1]:.25*day:pg.tspan[2],
                 stack_reorder =true,
                 ht0=ht0
                 )    

pg = RealGlacier(pg,
                 int_b_0_sm= Spline1D(runoff_date_mod,
                                      Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                        pg.b_0,
                                                        (Int(div(pg.Q_sm,2)*2+1),) # make odd
                                                        )
                                      ; w= ones(length(runoff_date_mod)), k=2, bc="nearest"),
                 int_b_0_sm_spin = Spline1D(runoff_date_spin,
                                            Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0_spin,
                                                              (Int(div(pg.Q_sm,2)*2+1),)
                                                              )
                                            ; w= ones(length(runoff_date_spin)), k=2, bc="nearest"),
                 
                 )

odeopts=Num().odeopts
odeopts[:saveat]= pg.tout
odeopts[:save_everystep ]= false
odeopts[:abstol]= 1e-8
odeopts[:reltol]= 1e-8
pn =Num(pn,
        odeopts=odeopts)

pp=Phys(pp,
        B=2.0509424314739124e-11
        )
error()
if run_type == "single"
    ref_no = 70
    @load "Parameters_clean.jld2" Dm_l B_l ht0
    
    run_name="$(glacier)_test_best"
    
    pp=Phys(pp,
            Dm=Dm_l[ref_no],
            B=B_l[ref_no])

    pg = RealGlacier(pg,
                     ht0=ht0[ref_no] .+zeros(pg.ncells)                     
                     )
    println(pg.tspan)
    println("starting run_model()")
    println(Dates.now())
    @time sol  = run_model(pg, pp, pn)
    println("run_model() finished")

    #######################
    ### Post Processing ###
    #######################

    @save "$(fp_save)$(run_name)_sol.jld2" sol pn pg pp 
    
    hts, dht_dts,sed_source, Qws, Qws_sm, Qbs, Qbes = SUGSET.save_run(sol, pg, pp, pn, run_name,fp_save;verbose=false)

elseif run_type == "parameter_search"

    include("gries_annual.jl")

    gries_data[end,2] = pg.tout[end]
    #         dm       B   ht0         
    # upper = [0.03     1e-10     5e-3 ]
    # lower = [1e-3      1e-12    1e-4 ]
    
    # no_try_runs = 2500

    # Dm_l=[]; B_l=[] ;ht0=[]; ϵ̇_l=[]

    # v1 = rand(MersenneTwister(123),Uniform(lower[1],upper[1]),       no_try_runs)# i
    # v2 = rand(MersenneTwister(456),LogUniform(lower[2],upper[2]),       no_try_runs)# j 
    # v3 = rand(MersenneTwister(789), LogUniform(lower[3],upper[3]),no_try_runs)# l
    # counter = 0
    # for i= 1:no_try_runs
        
    #   global  pp= Phys(pp,
    #                    B=v2[i])
        
    #     ϵ̇_ =[]
        
    #     tmp= SUGSET.vectorize_output(pg,pp,pn, pg.tout[1], SUGSET.source_till)*year
    #     ϵ̇=mean(tmp[pg.inpoly])
        
    #     if ϵ̇ > .1e-3 && ϵ̇ < 2.5e-3 # Rates with an error from delaney 2018
    #         global counter += 1
    #         println("$(counter) ... $(i) ... $(round(ϵ̇*1e3,digits=4))")
    #         push!(Dm_l,v1[i]);push!(B_l,v2[i]);push!(ht0,v3[i]);push!(ϵ̇_l,ϵ̇)
    #     end
    #     if length(Dm_l) >200
    #         break
    #     end
        
    # end
    
    # Dm_l=hcat(Dm_l...); B_l =hcat(B_l...);ht0=hcat(ht0);ϵ̇_l =hcat(ϵ̇_l...)
    # @save "Parameters_clean.jld2" Dm_l B_l ht0 ϵ̇_l

    include("grid_search_pmap.jl")
    grid_search_2D(pp, pg, pn,gries_data, runoff_date_spin, runoff_date_mod, fp_save)
end
