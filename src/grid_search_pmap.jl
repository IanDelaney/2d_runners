# right now 3 parameters are included
using Distributions
using Random,DelimitedFiles
using Distributed
if length(procs())==1
    addprocs(10)
end
@everywhere begin
    using Pkg
    Pkg.activate(".")
    using JLD2
    using Dierckx, Interpolations, Images
    using SUGSET, VAWTools, Parameters, Distributions, Random, DelimitedFiles
    using Dates, ImageFiltering
    using OffsetArrays,Statistics, DifferentialEquations,OrdinaryDiffEq
    import SUGSET:  zs,zb, source_water, width, source_till, thick, source_water_ddm,  day, year, Num, abs_error
    include("interface-real-glacier_2d.jl")
    include("comparison_functions.jl")
end

@eval @everywhere begin
    lower=$lower
    upper=$upper
    RealGlacier=$RealGlacier
    pg=$pg
    pn=$pn

    
end

@everywhere begin
    # const File_name = file_name
    #const Upper = upper
    #const Lower = lower
    const total_runs = 100
       
    @load "Parameters_clean.jld2" Dm_l B_l ht0
    
    const v_1 = Dm_l
    const v_2 = B_l
    const v_3 = ht0    

    
end

function grid_search_2D(pp, pg, pn,data,runoff_date_spin, runoff_date_mod, fp_save)
    # run many model runs
    pmap(nr->one_evaluation_2D(nr, pp, pg, pn,data,runoff_date_spin, runoff_date_mod,fp_save), 1:(total_runs);
         )
    
    return nothing
end

@everywhere begin
    function one_evaluation_2D(nr, pp, pg, pn,data,runoff_date_spin, runoff_date_mod,fp_save)
        wt = time_ns()

        #        missing_bits =[28 42 48 55 58 63 72 73 78 82 88 95 96 97 99]
        i=nr
        #if i in missing_bits
        println("starting simulation $nr of $(total_runs)")
        ## redefine variables

        pp = Phys(pp,
                  Dm = v_1[i],
                  B  = v_2[i]
                  )
        
        pg = RealGlacier(pg,
                         ht0  = v_3[i] .+ zeros(pg.ncells),
                         )
        
        runs = -999
        
        odeopts= Num().odeopts
        odeopts[:saveat]= pg.tout
        odeopts[:save_everystep]= false
        pn =Num(pn,
                odeopts=odeopts)

        pg=RealGlacier(pg,
                       tspan=pg.tspan_run
                       )

        println("
                    Dm = $(pp.Dm)
                    B    = $(pp.B)
                    Ht0  = $(mean(pg.ht0))
                                        ")
        
        #################
        ### Run Model ###
        #################

        println("starting run_model()")
        println(Dates.now())
        @time sol  = run_model(pg, pp, pn)
        println("run_model() finished")

        #######################
        ### Post Processing ###
        #######################

        if sol.t[end] == pg.tout[end]
            dt=pg.tout[2]-pg.tout[1]
            Qbs=zeros(pg.ncells,length(pg.tout))
            Qbs_catch=zeros(length(pg.tout))
            
            for (k,t) in enumerate(pg.tout)
                hts = sol(t)
                _, _, Qbs[:,k], _, pg_tmp= SUGSET.dht_dt_fn(hts, t, pg, pp, pn)
                Qbs_catch[k] = sum(-Qbs[pg_tmp.edges,k],dims=1)[]
            end

            ###########################
            ### Parse data and such ###
            ###########################
            summed_model_out = data_sums(data,Qbs_catch*dt,pg)
            abs_err = SUGSET.abs_error(data[:,3],summed_model_out)    
            rank_cor = corspearman(summed_model_out,data[:,3])

            
            println("finishing simulation $nr of $(total_runs)
                                                                        rank: = $(rank_cor)
                                                                        cost function: $(abs_err)
                                                                        walltime: $((time_ns()-wt)/1e9)")
            
            headers= [                "wall-time"    "'Dm'"   "'B'" "'ht0'"      "'rank_cor'" "'abs_err'"    "'fi'"    "'year_1'"         "'year_2'"           "'year_3'" "'year_4'" ]
            file = vcat(headers,[(time_ns()-wt)/1e9  pp.Dm    pp.B    v_3[i]        rank_cor     abs_err     pp.fi  summed_model_out[1] summed_model_out[2]  summed_model_out[3]     summed_model_out[4]      ])
            
            writedlm("$(fp_save)/$(pg.name)_results_run_$(i)_220422.txt", file,"\t")

            sol=[];Qbs=[];Qbs_catch=[];
        else
            file =["not completed"]

            headers= [             "'Dm'"   "'kg'" "'l_er'"       "'fi'" ]
            file = vcat(headers,[ pp.Dm    pp.Kg    pp.l_er         pp.fi ])

            writedlm("$(fp_save)/$(pg.name)_results_run_$(i)_220422_FAIL.txt", file,"\t")
        end


        #        end
        return nothing
    end
end


