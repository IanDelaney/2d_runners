using PyPlot
using VAWTools
using JLD2
using Images
import SUGSET: day,year
using StatsBase
ENV["PYTHON"]=""
using Parameters

#include("interface_is.jl")

if homedir()== "/Users/Ian"
    fp_load = "/Users/Ian/research/SUGSET-2D/model_outputs/"
    fp_save = "/Users/Ian/research/SUGSET-2D/figs/"
end

#################
### load data ###
#################

run_name="ice_sheet_30year_.25day_normal"
#run_name="ice_sheet_30year_tgl_var_0mt_tol-10_B4"

@load "$(fp_load)$(run_name).jld2" pg pp pn hts dht_dts sed_source Qws Qws_sm Qws_src Qbs Qbes zs_ zb_


tout=pg.tout
edges=pg.edges
nh=pg.nh
ns=pg.ns
sv=pg.sv
hv=pg.hv

# Qb_day=zeros(Int(floor(length(tout)/4))-1)
# Qbes_day=zeros(Int(floor(length(tout)/4))-1) 

# for i = 1:Int(floor(length(tout)/4))-1
#     #    println(i)
#     ind=Int(i*4)
#     Qb_day[i] = sum(-Qbs[edges,ind:ind+4]*(pg.tout[2]-pg.tout[1]))
#     Qbes_day[i] = sum(-Qbes[edges,ind:ind+4]*(pg.tout[2]-pg.tout[1]))       
# end


Qb_day_mean  = mean(reshape(sum(-Qbs[edges,1:end-1],dims=1)',4,:),dims=2)
Qbes_day_mean  = mean(reshape(sum(-Qbes[edges,1:end-1],dims=1)',4,:),dims=2)
Qw_day_mean = mean(reshape(sum(-Qws[edges,1:end-1],dims=1)',4,:),dims=2)
dhdt_day_mean = mean(reshape(mean(dht_dts[edges,1:end-1],dims=1)',4,:),dims=2)

Qb_no_season = (reshape(sum(-Qbs[edges,1:end-1],dims=1)',4,:) .- Qb_day_mean)[:]
Qbes_no_season = (reshape(sum(-Qbes[edges,1:end-1],dims=1)',4,:) .- Qbes_day_mean)[:]
Qw_no_season = (reshape(sum(-Qws[edges,1:end-1],dims=1)',4,:) .- Qw_day_mean)[:]
dhdt_no_season= (reshape(mean(dht_dts[edges,1:end-1],dims=1)',4,:) .- dhdt_day_mean)[:]


Qb_day= zeros(Int(round(length(tout)/4)))
Qbes_day= zeros(Int(round(length(tout)/4)))
Qw_day= zeros(Int(round(length(tout)/4)))
dhdt_day= zeros(Int(round(length(tout)/4)))

Qb_no_season_day= zeros(Int(round(length(tout)/4)))
Qbes_no_season_day= zeros(Int(round(length(tout)/4)))
Qw_no_season_day= zeros(Int(round(length(tout)/4)))
dhdt_no_season_day = zeros(Int(round(length(tout)/4)))

for i = 1:Int(floor(length(tout)/4))
    #    println(i)
    if i== 1
        inda= i
    else
        inda= (i-1)*4
    end
    indb=Int(inda+4)
    Qb_day[i] = (tout[2]-tout[1]) * sum(-Qbs[edges,inda:indb])
    Qbes_day[i] = (tout[2]-tout[1]) * sum(-Qbes[edges,inda:indb])
    Qw_day[i] = (tout[2]-tout[1]) * sum(-Qws[edges,inda:indb])
    dhdt_day[i] = (tout[2]-tout[1]) * mean(dht_dts[edges,inda:indb])
    
    Qb_no_season_day[i] = (tout[2]-tout[1]) * sum(Qb_no_season[inda:indb])
    Qbes_no_season_day[i] = (tout[2]-tout[1]) * sum(Qbes_no_season[inda:indb])
    Qw_no_season_day[i] = (tout[2]-tout[1]) * sum(Qw_no_season[inda:indb])
    dhdt_no_season_day[i] = (tout[2]-tout[1]) * sum(dhdt_no_season[inda:indb]) 

end


######################
### start plotting ###
######################
#figure(figsize=(10,10))
#fig, axs = subplots(2,1,figsize=(7,5))
#fs=12


fs=12

fig1 = figure(figsize=(10,4))

################################
################################
ax1 = plt.subplot2grid((2,2), (0,0), colspan=2)
axvspan(10, 20,color="lightgray")
p1=plot(tout[1:4:end]./year, sum(-Qws[edges,1:4:end],dims=1)' ,linewidth="2", color="steelblue", label = "Water Discharge" )
#xlabel("Year", fontsize=fs)
title("Model with 30 years of forcing", fontsize=fs)
ylabel(L"Q_{w}~(m³\,s⁻¹)",color="steelblue", fontsize=fs) 
#legend()
xticks([])
text(1,2625, "a",fontsize=fs*1.5)
xlim([0.0, 30])

ax1b = twinx()
#p2=semilogy
p2=plot(tout[1:4:end]./year,mean(hts[:,1:4:end],dims=1)',linewidth="2", color="darkorange", label = "Erosion rate")
ylabel("mean H (m)",color="darkorange", fontsize=fs)

#ylim([1e-4, 5e-3])
#legend()

xlim([0.0, 30])
#ylim([0.0700, 0.072])
xticks([])
yticks([0.07050, 0.071, 0.0715])

#################################
ax1 = plt.subplot2grid((2,2), (1,0), colspan=2)
### set Hspace = 0
subplots_adjust(hspace=-0.0)

plot(tout[1:4:end]./year, sum(-Qbes[edges,1:4:end],dims=1)' ,linewidth="2", color="purple", label = " Sediment discharge capacity " )
#plot(tout[8:4:end]./year, 1000* Qb_day.*365 ./(1e5*20000) ,linewidth="2", color="purple")
xlabel("Year", fontsize=fs)
ylabel(L"Q_{sc}~(m³\,s⁻¹)",color="purple", fontsize=fs)
xlim([0.0, 30])
axvspan(10, 20,color="lightgray")

#legend()

twinx()
plot(tout[1:4:end]./year,sum(-Qbs[edges,1:4:end],dims=1)',linewidth="2", color="forestgreen", label ="Sediment discharge" )
xlabel("Year", fontsize=fs)
ylim([0.062, 0.0640])
text(1,0.0635, "b",fontsize=fs*1.5)
ylabel(L" Q_{s}~(m³\, s⁻¹)",color="forestgreen", fontsize=fs)
xlim([0.0, 30])
#legend()

tight_layout()
savefig("$(fp_save)ice_sheet_ts.pdf")

#####################################################################################
fig, axs = subplots(2,2,figsize=(10,4))
fs=12

ax1=sca(axs[1])
#subplot(2,2,1)
im1=imshow(reshape(abs.(-Qbs[:,end]), nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im1,shrink=.25,pad=.05, aspect=10)
#ax3.set_label("Sediment discharge (m³ s⁻¹)")
ylabel("y (m)");xlabel("x (m)")
title("Sediment discharge (m³ s⁻¹) at year $(round(tout[end]/year,digits=0))", fontsize=fs)

ax2=sca(axs[2])
#subplot(2,2,2)
im2=imshow(reshape(abs.(-Qws[:,end]), nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im2,shrink=.25,pad=.05,aspect=10)
#ax3.set_label("Sediment discharge (m³ s⁻¹)")
ylabel("y (m)");xlabel("x (m)")
plt.title("Water discharge (m³ s⁻¹) at year $(round(tout[end]/year,digits=0))", fontsize=fs)

ax3=sca(axs[3])
#subplot(2,2,3)
im3=imshow(reshape(hts[:,1], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im3,shrink=.25,pad=.05,aspect=10);clim([1.2e-1, 0])
 ylabel("y (m)");xlabel("x (m)")
title("Till-layer height (m) at year $(round(tout[1]/year,digits=0))", fontsize=fs)

ax4=sca(axs[4])
#subplot(2,2,4)
im4=imshow(reshape(hts[:,end], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "gist_earth");colorbar(im4,shrink=.25,pad=.05,aspect=10);clim([1.2e-1, 0])
ylabel("y (m)");xlabel("x (m)")
title("Till-layer height (m) at year $(round(tout[end]/year,digits=0))", fontsize=fs)

subplots_adjust(hspace=-0.6)
tight_layout()
savefig("$(fp_save)ice_sheet_spatial.pdf")
