using Parameters
using PyPlot
import Roots: find_zero
using StatsBase
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,startup,b_0_determ


#glacier = "gries";experiment="annual";
#fp ="/Users/Ian/data/sed_modeling_data/gries/"
glacier = "gorner";experiment="adsafasdnnual"; fp ="/Users/Ian/data/sed_modeling_data/gorner/"

if homedir()== "/Users/Ian"
    fp ="/Users/Ian/data/sed_modeling_data/$(glacier)/"
    fp_save = "/Users/Ian/data/sed_modeling_data/$(glacier)/"

end

include("real_glacier_processing_2D.jl")
error()
#Qw_read_in = readdlm("$(fp)gries_h_2009_2016.dat",',',String) # read in runoff

#Qw, runoff_date_mod   = runoff_b0_sort!(Qw_read_in)

pp=Phys(Dm=0.10,
        hookeangle= pi/10)

pn=Num(odeopts=odeopts,
       Δσ=1e-3)

b_0= b_0_determ(Qw,pg)

fp="/Users/ian/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's computer

######################
### write to file  ###
######################

headers= ["'date-time'" "'b_0'"]
data_bits= [unix2datetime.(runoff_date_mod) b_0]
file = vcat(headers,data_bits)


plot(data_bits[:,1],data_bits[:,2])
writedlm("$(fp)$(glacier)_2d_b_0.dat",file, ',')

time = collect(7:.25:16)
Qw=LinRange(16,26,  37)
const b_0= b_0_determ(Qw,pg)


function mass_bal_grad(s,h,t,pg)
    (b0[end-7] +  pg.gamma *(zs(s,h,t,pg)-pg.zs_min))* SUGSET.is_inpoly(s,h,pg)
end
