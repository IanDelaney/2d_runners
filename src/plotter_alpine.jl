using PyPlot
using VAWTools
using JLD2
using Images
using FFTW
using SUGSET
import SUGSET: day,year,Valley
using StatsBase
ENV["PYTHON"]=""
using Parameters

run_type = "tmp"
include("interface_alpine.jl")

pp=Phys()

pg=Valley(tspan=(0,1))
pn=Num(Δσ=1e-3)
pg,pp,pn = SUGSET.startup(pg, Valley, pp,pn)
ub = SUGSET.vectorize_output(pg,pp,pn, 0.0,SUGSET.erosion).*year.*1e3

if homedir()== "/Users/Ian"
    fp_load =  "/Users/Ian/research/SUGSET-2D/model_outputs/"
    fp_save =  "/Users/Ian/research/SUGSET-2D/figs/"
end

#################
### load data ###
#################
run_name="alpine_30years_tgl_novar_DT5"
#run_name="gries_test_best_"

@load "$(fp_load)$(run_name).jld2" pg pp pn hts dht_dts sed_source Qws Qws_sm Qws_src Qbs Qbes zs_ zb_

tout=pg.tout
edges=pg.edges
nh=pg.nh
ns=pg.ns
sv=pg.sv
hv=pg.hv

Qb_year_mean  = mean(reshape(sum(-Qbs[edges,1:end-1],dims=1)',1460,:),dims=2)
Qbes_year_mean  = mean(reshape(sum(-Qbes[edges,1:end-1],dims=1)',1460,:),dims=2)
Qw_year_mean = mean(reshape(sum(-Qws[edges,1:end-1],dims=1)',1460,:),dims=2)
dhdt_year_mean = mean(reshape(mean(dht_dts[edges,1:end-1],dims=1)',1460,:),dims=2)

Qb_no_season = (reshape(sum(-Qbs[edges,1:end-1],dims=1)',1460,:) .- Qb_year_mean)[:]
Qbes_no_season = (reshape(sum(-Qbes[edges,1:end-1],dims=1)',1460,:) .- Qbes_year_mean)[:]
Qw_no_season = (reshape(sum(-Qws[edges,1:end-1],dims=1)',1460,:) .- Qw_year_mean)[:]
dhdt_no_season= (reshape(mean(dht_dts[edges,1:end-1],dims=1)',1460,:) .- dhdt_year_mean)[:]

Qb_year= zeros(Int(round(length(tout)/1460)))
Qbes_year= zeros(Int(round(length(tout)/1460)))
Qw_year= zeros(Int(round(length(tout)/1460)))
dhdt_year= zeros(Int(round(length(tout)/1460)))
hts_year= zeros(Int(round(length(tout)/1460)))

Qb_no_season_year= zeros(Int(round(length(tout)/1460)))
Qbes_no_season_year= zeros(Int(round(length(tout)/1460)))
Qw_no_season_year= zeros(Int(round(length(tout)/1460)))
dhdt_no_season_year = zeros(Int(round(length(tout)/1460))) 

for i = 1:Int(floor(length(tout)/1460))
    #    println(i)
    if i== 1
        inda= i
    else
        inda= (i-1)*1460
    end
    indb=Int(inda+1460)
    Qb_year[i] = (tout[2]-tout[1]) * sum(-Qbs[edges,inda:indb])
    Qbes_year[i] = (tout[2]-tout[1]) * sum(-Qbes[edges,inda:indb])
    Qw_year[i] = (tout[2]-tout[1]) * sum(-Qws[edges,inda:indb])
    dhdt_year[i] = (tout[2]-tout[1]) * mean(dht_dts[edges,inda:indb])
    hts_year[i] =  mean(hts[:,inda:indb])
    
    Qb_no_season_year[i] = (tout[2]-tout[1]) * sum(Qb_no_season[inda:indb])
    Qbes_no_season_year[i] = (tout[2]-tout[1]) * sum(Qbes_no_season[inda:indb])
    Qw_no_season_year[i] = (tout[2]-tout[1]) * sum(Qw_no_season[inda:indb])
    dhdt_no_season_year[i] = (tout[2]-tout[1]) * sum(dhdt_no_season[inda:indb]) 

end




######################
### start plotting ###
######################

fs=12

fig1 = figure(figsize=(12,5))

#################################
ax1 = plt.subplot2grid((2,2), (0,0), colspan=2)

axvspan(10, 20,color="lightgray")

title("Model with 30 years of forcing", fontsize=fs)
axvspan(12, 20,color="lightgray")
p1=plot(tout[1:4:end]./year, sum(-Qws[edges,1:4:end],dims=1)' ,linewidth="2", color="steelblue", label = "Water discharge" )
xlim([7,25])
text(7.5,17.5, "a",fontsize=fs*1.5)
ylim([0.5,maximum(sum(-Qws[edges,1:4:end],dims=1)')])

ylabel(L"Q_{w}~(m³ s⁻¹)",color="steelblue", fontsize=fs)
xticks([])

ax1b = twinx()
plot(1:30, hts_year,linewidth="2", color="grey", label = "Annual")
p2=plot(tout/year, mean(hts,dims=1)',linewidth="2", color="darkorange", label = "Till height")
#yticks([0.021, 0.022, 0.024])
ylabel("mean H (m)",color="darkorange", fontsize=fs,rotation=270,labelpad=15)
ylim([0.0252,0.035])
text(2009.5,.23, "a",fontsize=fs*1.5)
xticks([])

subplots_adjust(hspace=0.0)
###############################################
ax2 = plt.subplot2grid((2,2), (1,0), colspan=2)
###############################################


axvspan(10, 20,color="lightgray")
plot(tout[1:4:end]./year,sum(-Qbs[edges,1:4:end],dims=1)',linewidth="2", color="chocolate", label ="Sediment discharge" )
xlim([7,25])
xlabel("Year", fontsize=fs)
ylim([0.00001, 0.0125])
ylabel(L" Q_{s}~(m³ s⁻¹)",color="chocolate", fontsize=fs)


twinx()



semilogy(tout[730:1460:end-730]./year, Qb_year ,linewidth="2", color="forestgreen", label = " Sediment discharge capacity " )
#ylim([1e5, 7e5])
xlabel("Year", fontsize=fs)
ylabel(L"Q_{s} ~ (m³ a⁻¹)",color="forestgreen", fontsize=fs,rotation=270,labelpad=15)
text(7.5,7e3, "b",fontsize=fs*1.5)
xlabel("Year", fontsize=fs)

savefig("$(fp_save)alpine_ts.pdf")


#####################################################################################
fig, axs = subplots(3,2,figsize=(12,5))
fs=11


ax1=sca(axs[1])
im1=imshow(reshape(max.(05,zs_ .- zb_) , nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im1,shrink=.4,pad=.05, aspect=10, label="(m)")
ylabel("y (m)");xlabel("x (m)"); 
title("Ice thickness", fontsize=fs)

ax2=sca(axs[2])
im2=imshow(reshape(-Qbs[:,20075-250], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im2,shrink=.4,pad=.05, aspect=10, label="(m³ s⁻¹)")
ylabel("y (m)");xlabel("x (m)")
title("Sediment discharge  at year $(round(tout[20072-250]/year,digits=2))", fontsize=fs)

ax3=sca(axs[3])
im3=imshow(reshape(-Qws[:,20075-250], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im3,shrink=.4,pad=.05, aspect=10, label="(m³ s⁻¹)")
#ax3.set_label("Sediment discharge (m³ s⁻¹)")
ylabel("y (m)");xlabel("x (m)")
plt.title("Water discharge  at year $(round(tout[20072-250]/year,digits=2))", fontsize=fs)

ax4=sca(axs[4])
im4=imshow(reshape(ub, nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im4,shrink=.4,pad=.05, aspect=10, label=" (mm a⁻¹)")
ylabel("y (m)");xlabel("x (m)")
title("Glacier erosion rate", fontsize=fs)

ax5=sca(axs[5])
im5=imshow(reshape(hts[:,1460*10], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im5,shrink=.4,pad=.05, aspect=10, label="(m)");clim([0.001,maximum(hts)])
ylabel("y (m)");xlabel("x (m)")
title("Till-layer height at year $(round(tout[1460*10]/year,digits=2))", fontsize=fs)

ax6=sca(axs[6])
im6=imshow(reshape(hts[:,1460*29], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)],cmap = "viridis");colorbar(im6,shrink=.4,pad=.05, aspect=10, label="(m)");clim([0.001,maximum(hts)])
title("Till-layer height at year $(round(tout[1460*29]/year,digits=2))", fontsize=fs)
ylabel("y (m)");xlabel("x (m)")
subplots_adjust(hspace=-0.3)
tight_layout()

savefig("$(fp_save)alpine_spatial.pdf")
