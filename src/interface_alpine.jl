using RandomNumbers
function SUGSET.source_water(s,h,t,pg::Valley,pp)

    Δ_yr=0.5
    step_1 = 10
    step_2 = 20
    @assert step_2 > step_1

    rng=RandomNumbers.Xorshifts.Xoroshiro128Star(Int(floor((t)/SUGSET.year,digits=0)+year))
    clima_var = 0.0#randn(rng)

    #clima_var=0
    change_grad = Δ_yr/year

    if t < step_1*year
        ΔT = pp.ΔT + clima_var
    elseif t >= step_1*year && t <step_2*year
        ΔT = pp.ΔT + clima_var + change_grad*(t-step_1*year)
    elseif t >= step_2*year
        ΔT = pp.ΔT + clima_var + change_grad*(step_2-step_1)*year
    end

    pp = Phys(pp,
              ΔT=ΔT  
              )
    
    return source_water_ddm(s,h,t,pg,pp)
end



if run_type== "homogen_erosion" 
    SUGSET.erosion(s,h,t,pg::Valley,pp)=2e-3/year
    
elseif run_type== "seasonal_erosion" 
    """
    source_till(s, h, t, pg::Glacier, pp::Phys)

    Till production term in m/s
    """
    function  SUGSET.source_till(s,h,t,pg,pp)
        if SUGSET.source_water(s,h,t,pg,pp) > 5*pp.basal
            return SUGSET.erosion(s,h,t,pg,pp)    
        else
            return 0.0
        end
    end
end

