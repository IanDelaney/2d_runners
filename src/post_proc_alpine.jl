using Parameters
using Random
using PyPlot
using JLD2
using DifferentialEquations,OrdinaryDiffEq
import Roots: find_zero
using StatsBase
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET:  zs,zb, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,Num,startup

if homedir()== "/Users/Ian"
    fp_save = "/Users/Ian/research/SUGSET-2D/model_outputs/"
elseif homedir()=="/home/tesla-k20c"
   fp_save="/home/tesla-k20c/ssd/iand/outputs/"
end

run_name="alpine_10years_sol_test_tol-6.jld"
include("interface_alpine.jl")

@load "$(fp_save)$(run_name)" sol pn pg pp

run_name="alpine_10years_test_tol-6.jld"

pg=Valley(pg,tout=(pg.tout[1]:0.25*day:pg.tout[end]))

hts, dht_dts,sed_source, Qws, Qws_sm, Qbs, Qbes = SUGSET.save_run(sol, pg, pp, pn, run_name,fp_save;verbose=false)

error()


