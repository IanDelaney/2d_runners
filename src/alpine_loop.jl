using PyPlot
#using Plots
using VAWTools
using JLD2
using Images
using FFTW
import SUGSET: day,year
using StatsBase
ENV["PYTHON"]=""
using Parameters

plt =PyPlot

if homedir()== "/Users/Ian"
    fp_load = "/Users/Ian/research/SUGSET-2D/model_outputs/"
    fp_save = "/Users/Ian/research/SUGSET-2D/figs/"
end

#################
### load data ###
#################
run_name="alpine_30years_tgl_var"
@load "$(fp_load)$(run_name).jld2" pg pp pn hts dht_dts sed_source Qws Qws_sm Qws_src Qbs Qbes zs_ zb_


tout=pg.tout
edges=pg.edges
nh=pg.nh
ns=pg.ns
sv=pg.sv
hv=pg.hv

# Qb_day=zeros(Int(floor(length(tout)/4)))
# for i = 1:Int(floor(length(tout)/4))
#     #    println(i)
#     ind=Int(i*10)
#     Qb_day[i] = sum(-Qbs[edges,ind:ind+4])/4

# end

Qb_year= zeros(Int(round(length(tout)/1460)))

for i = 1:Int(floor(length(tout)/1460))
    #    println(i)
    if i== 1
        inda= i
    else
        inda= (i-1)*1460

    end
    
    indb=Int(inda+1460)
    Qb_year[i] = (tout[2]-tout[1]) * sum(-Qbs[edges,inda:indb])
end


#################################
run_name="alpine"
#run_name="ice_sheet"
fs=12

fig1 = plt.figure(figsize=(11,7))
Threads.@threads for  i = 16350:18688
    println(tout[i]./year)
    #i=19000
    ax1 = plt.subplot2grid((11,2), (0,0),rowspan=2, colspan=2)
    p1=plt.plot(tout[16350:i]./year , sum(-Qws[edges,16350:i],dims=1)' ,linewidth="2", color="steelblue", label = "Water discharge" )
    plot(tout[i]./year , sum(-Qws[edges,i],dims=1)' ,marker="o", color="black", label = "Water discharge" )
    plt.xticks([])
    ylim([0, 13.7])
    plt.xlim([11.15, 12.8])
    xlabel("Year", fontsize=fs)
    plt.ylabel(L"Q_{w}~(m³ s⁻¹)",color="steelblue", fontsize=fs)
    #legend()

    ax1b = plt.twinx()
    #p2=
    plt.semilogy(tout[16350:i]./year, mean(hts[:,16350:i],dims=1)',linewidth="2", color="darkorange", label = "Till height")
    plot(tout[i]./year , mean(hts[:,i],dims=1)',marker="o", color="black", label = "Water discharge" )
    plt.ylabel("mean H (m)",color="darkorange", fontsize=fs) 
    ylim([3.27e-2, 3.31e-2])
    #legend()
    plt.xlim([11.15, 12.8])
    plt.xticks([])
    #plt.axvline(tout[i]/year,linewidth="3",color="black")
    #lns=p1+p2
    #labs = [l.get_figlabel()[1] for l in lns]
    #ax.legend(lns, labs, loc=0)

    plt.subplots_adjust(hspace=0.0)

    ax2 = plt.subplot2grid((11,2), (2,0), rowspan=2, colspan=2)
    plt.plot(tout[16350:i]./year, sum(-Qbs[edges,16350:i],dims=1)',linewidth="2", color="forestgreen", label ="Sediment discharge" )
    plot(tout[i]./year , sum(-Qbs[edges,i],dims=1)',marker="o", color="black", label = "Water discharge" )
    plt.xlabel("Year", fontsize=fs)
    plt.ylim([0.0, 0.00057])
    plt.xlim([11.15, 12.8])
    plt.ylabel(L" Q_{s}~(m³ s⁻¹)",color="forestgreen", fontsize=fs)
    #plt.axvline(tout[i]./year,linewidth="3",color="black")
    
    
    ax4 = plt.subplot2grid((11,2), (6,0),rowspan=2, colspan=2)
   
    im1=plt.imshow(reshape(-Qbs[:,i], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)]);
    plt.colorbar(im1,shrink=.5,pad=.05, aspect=1)
    plt.clim([0, 0.001])
    #ax3.set_label("Sediment discharge (m³ s⁻¹)")
    plt.title("Sediment discharge (m³ s⁻¹) at year $(round(tout[i]/year,digits=2))", fontsize=fs)


    ax5 = plt.subplot2grid((11,2), (9,0),rowspan=2, colspan=2)
    im2=plt.imshow(reshape(-Qws[:,i], nh,ns),extent=[minimum(sv),maximum(sv),minimum(hv),maximum(hv)]);colorbar(im2,shrink=.5,pad=.05,aspect=1)
    plt.clim([0, maximum(-Qws)*.95])
    plt.title("Water discharge (m³ s⁻¹) at year $(round(tout[i]/year,digits=2))", fontsize=fs)
    plt.subplots_adjust(hspace=0.0)
    #tight_layout()

    #plt.pause(0.0001)
    savefig("/Users/Ian/research/SUGSET-2D/model_outputs/video/alpine_video/$(run_name)_video_$(i).png")
    
    plt.clf()
end
error()
#gif(anim, "test_anim_fps30.gif", fps = 30)


########
using FileIO, ProgressMeter, Images
imgnames = filter(x->occursin(".png",x),readdir("/Users/Ian/research/SUGSET-2D/model_outputs/video/is_video/")) # Populate list of all .pngs
intstrings_ =  map(x->split(x,".")[1],imgnames) # Extract index from filenames
intstrings =  map(x->split(x,"_")[4],intstrings_) # Extract index from filenames
p = sortperm(parse.(Int,intstrings)) #sort files numerically
imgstack = []
@showprogress for i=1:length(imgnames) #in imgnames[p]
    #println(imgnames[i])
    push!(imgstack,RGB.(load("/Users/Ian/research/SUGSET-2D/model_outputs/video/is_video/$(imgnames[i])")))
end

using VideoIO
props = [:priv_data => ("crf"=>"0","preset"=>"ultrafast")]
encodevideo("/Users/Ian/research/SUGSET-2D/model_outputs/video/is_video/alpine_video.mp4",imgstack[1:11000],   framerate=4,AVCodecContextProperties=props)
         

