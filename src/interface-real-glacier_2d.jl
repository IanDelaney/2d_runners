\#################################
### create glacier parameters ###
#################################
using SUGSET, Parameters, Dierckx, Interpolations, Images
using SUGSET: zs, zb, erosion

###############

const InterpType = Interpolations.GriddedInterpolation{Float64,1,Float64,Interpolations.Gridded{Linear},Tuple{Array{Float64,1}}}

const SgridType  = StepRangeLen{Float64,Base.TwicePrecision{Float64},Base.TwicePrecision{Float64}}

const Interpgeo_type = ScaledInterpolation{Float64, 2, Interpolations.Extrapolation{Float64, 2, Interpolations.BSplineInterpolation{Float64, 2, OffsetMatrix{Float64, Matrix{Float64}}, BSpline{Quadratic{Line{OnGrid}}}, Tuple{Base.OneTo{Int64}, Base.OneTo{Int64}}}, BSpline{Quadratic{Line{OnGrid}}}, Flat{Nothing}}, BSpline{Quadratic{Line{OnGrid}}}, Tuple{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}, Int64}, StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}, Int64}}}

const Interpgeo_type_surf = Interpolations.Extrapolation{Float64, 2, Interpolations.GriddedInterpolation{Float64, 2, Float64, Interpolations.Gridded{Linear{Throw{OnGrid}}}, Tuple{Vector{Float64}, Vector{Float64}}}, Interpolations.Gridded{Linear{Throw{OnGrid}}}, Flat{Nothing}}


const Interpgeo_type_thick= Interpolations.Extrapolation{Float64,2,Interpolations.BSplineInterpolation{Float64,2,OffsetArray{Float64,2,Array{Float64,2}},BSpline{Quadratic{Line{OnGrid}}},Tuple{Base.OneTo{Int64},Base.OneTo{Int64}}},BSpline{Quadratic{Line{OnGrid}}},Flat{Nothing}}

const Interpgeo_type_b =  Interpolations.Extrapolation{Float64, 2, Interpolations.GriddedInterpolation{Float64, 2, Bool, Interpolations.Gridded{Constant{Nearest, Throw{OnGrid}}}, Tuple{Vector{Float64}, Vector{Float64}}}, Interpolations.Gridded{Constant{Nearest, Throw{OnGrid}}}, Flat{Nothing}}



"Defines Real glacier"
@with_kw struct RealGlacier <: Glacier @deftype Float64
    name::String # glacier name

    # The spatial domain:
    domain_s::Tuple{Float64, Float64}=(minimum(bed_dem.x), maximum(bed_dem.x))
    domain_h::Tuple{Float64, Float64}=(maximum(bed_dem.y), minimum(bed_dem.y))

    #GRIES
    ns::Int = Int(floor(119))
    nh::Int = Int(floor(101))

    nnodes::Int = (ns+1)*(nh+1)
    ncells::Int = ns*nh
    stack_nnodes::Int= ns*nh

    ds = (domain_s[2]-domain_s[1])./(ns-1)
    dh = (maximum(domain_h)-minimum(domain_h))./(nh-1)

    
    sgrid::LinRange{Float64}=range(domain_s[1],stop=domain_s[2],length=Int(ns +1))
    hgrid::LinRange{Float64}=range(domain_h[1],stop=domain_h[2],length=Int(nh +1))

    smid::LinRange{Float64}=range(domain_s[1],stop=domain_s[2],length=ns) .+ds*0.5
    hmid::LinRange{Float64}=range(domain_h[1],stop=domain_h[2],length=nh) .+dh*0.5


    sv::Array{Float64}=zeros(ncells)
    hv::Array{Float64}=zeros(ncells)
    
    # Time domain
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}
    tspan_run::Tuple{Float64, Float64}
    tout::Vector{Float64}=tspan[1]:SUGSET.day:tspan[2] # output times
    
    @assert tout[1]>=tspan[1]
    @assert tout[2]<=tspan[2]

    
    

    
    # Routing bits
    route_type::String = "multiple"
    stack_reorder =true
    receiver::Array{Int64}=zeros(4,ncells)
    donor::Array{Int64}=zeros(4,ncells)
    ndon::Array{Int64}=zeros(1,ncells)
    stack::Array{Int64}=zeros(1,ncells)
    nreceiver::Array{Int64}=zeros(1,ncells)
    wrec::Array{Float64}= zeros(2,ncells)
    edges::Array{Int64}=zeros(1,ncells)
    is_in_poly::Array{Int64}= ones(ncells)

    BC::String = "none"
    
    # IC & BC
    ht0::Vector{Float64}=zeros(ncells) .+ 0.1 # initial till layer thickness
    Q_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top

    # source averaging
    source_average_time=day*4.5
    source_quantile = 0.2
    

    #till
    till_growth_lim = 0.05 # maximium amount of till, above which till stops being produced.
    fsl = 1.0# fraction of basal sliding
    till_lim = 0.1

    
    # Topo
    para = 0.05
    spin::Bool = false
    spin_fac = 1
    # misc parameters
    float_fracˣ = 0.99
    float_frac::Vector{Float64} = float_fracˣ .+ 0.0*ht0 # 1<=>overburden pressure, 0<=>atmospheric
    
    #@assert 0<=float_frac<1

    gamma= -0.0008./year # mass balance gradient per second
    B_0_term = 0

    Q_sm = 24*4 #smoothing factor for discharge  

    hydro_type::String=["real","ddm"][1]

    # mass balance at terminus
    b_0::Array{Float64} = boxcar(runoff_b0_sort!(b_0_read_in)[1],3)
    b_0_sm::Array{Float64} = zeros(length(b_0_read_in))
    b_0_spin::Array{Float64} = boxcar(runoff_b0_sort!(b_0_spin_read_in)[1],3)
    b_0_sm_spin::Array{Float64} = zeros(length(b_0_read_in))


    inpoly::Array{Int64} =[0, 0]
    
    ####
    int_b_0::Dierckx.Spline1D         = Spline1D(runoff_date_mod, b_0;      w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11)
    int_b_0_sm::Dierckx.Spline1D = Spline1D(runoff_date_mod,
                                            Images.mapwindow( v->quantile(v, source_quantile),
                                                              b_0,
                                                              (Int(div(Q_sm,2)*2+1),) # make odd
                                                              )
                                            ; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11
                                            )

    
    int_b_0_spin::Dierckx.Spline1D    = Spline1D(runoff_date_spin,b_0_spin; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11)
    int_b_0_sm_spin::Dierckx.Spline1D = Spline1D(runoff_date_spin,
                                                 Images.mapwindow( v->quantile(v, source_quantile),
                                                                   b_0_spin,
                                                                   (Int(div(Q_sm,2)*2+1),)
                                                                   )
                                                 ; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11
                                                 )
    ####
    # Topo
    spl_surf::Interpgeo_type_surf=spl_surf
    spl_bed::Interpgeo_type=spl_bed
    spl_inpoly::Interpgeo_type_b= spl_inpoly
    zs_min = 0.0
end

#########################
### shape data
#########################
SUGSET.is_inpoly(s,h, pg)= pg.spl_inpoly(s,h)

"""
    makes surface
    """
function SUGSET.zs(s,h,t, pg::RealGlacier)        
    out = pg.spl_surf(s,h)
    if isnan(out)
        out =pg.spl_bed(s,h)
    end
    
    return out
    
end

"""
    makes bed
    """
function SUGSET.zb(s,h,t, pg::RealGlacier) 
    out = pg.spl_bed(s,h)
    if isnan(out)
        out =  pg.spl_surf(s,h)
    end
    
    return out
end

function SUGSET.thick(s,h,t, pg::RealGlacier)
    out = max( SUGSET.zs(s,h,t, pg::RealGlacier) - SUGSET.zb(s,h,t, pg::RealGlacier) ,0.0)*SUGSET.is_inpoly(s,h, pg)    
end


"""
    source_water_meas_Q_sm(s,t,pg,pp,pd)
    smoothed souce of water for a given cell
    """
function source_water_meas_Q_sm(s,h,t,pg,pp)
    if pg.spin == true
        out = max.(pp.basal, Dierckx.evaluate(pg.int_b_0_sm, t) .+ pg.gamma .*(zs(s,h,t,pg)-pg.zs_min))* SUGSET.is_inpoly(s,h,pg) 
    elseif  pg.spin == false
        out = max.(pp.basal, Dierckx.evaluate(pg.int_b_0_sm, t) .+ pg.gamma .*(zs(s,h,t,pg)-pg.zs_min))* SUGSET.is_inpoly(s,h,pg)

    else
        error("spin or not!!!!")
    end
end

"""
    source_water_meas_Q(s,t,pg,pp,pd)
    souce of water for a given cell
    """
function source_water_meas_Q(s,h,t,pg,pp)

    if pg.spin == true
        out = max.(pp.basal, Dierckx.evaluate(pg.int_b_0,t) .+ pg.gamma .*(zs(s,h,t,pg)-pg.zs_min))* SUGSET.is_inpoly(s,h,pg)
    elseif pg.spin == false
        out = max.(pp.basal, Dierckx.evaluate(pg.int_b_0,t)    .+ pg.gamma .*(zs(s,h,t,pg)-pg.zs_min))* SUGSET.is_inpoly(s,h,pg)
        
    else
        error("spin or not!!!!")
    end

end

##############################
### water and till sources ###
##############################

function SUGSET.source_water_sm(s,h,t,pg::RealGlacier,pp)
    source_water_meas_Q_sm(s,h,t,pg,pp) # smoothed discharge for channel size parameterization
end

function SUGSET.source_water(s,h,t,pg::RealGlacier,pp)
    
    if pg.hydro_type=="real"
        source_water_meas_Q(s,h,t,pg,pp)  # actual discharge for determining water velocity
    elseif pg.hydro_type=="ddm"
        SUGSET.source_water_ddm(s,h,t,pg,pp)* SUGSET.is_inpoly(s,h,pg)
    else
        error()
    end
end

SUGSET.source_till(s,h,t,pg,pp) = SUGSET.erosion_herman(s,h,t,pg,pp)#* SUGSET.is_inpoly(s,h,pg)

##############################
### Routing  ###
##############################


function SUGSET.add_spatial_vec(GlacierType::DataType,pg)
    sv=zeros(pg.ncells);hv=zeros(pg.ncells)
    
    for j = 1:pg.ns
        for i = 1:pg.nh
            ij = SUGSET.ij_return(i,j,pg.nh)            
            sv[ij]=pg.smid[j];hv[ij]=pg.hmid[i]
        end
    end
    
    pg=RealGlacier(pg,
                   sv=sv,
                   hv=hv)
    return pg
end


function SUGSET.return_stack_with_ff(Dh,Q,t,pg::RealGlacier,pp)

    Φ_r, Φ =SUGSET.Φ_eval(Dh,Q,t,pg,pp)
    
    tmp =   findall(!iszero, Q)

    pg = typeof(pg)(pg,
                    float_frac= min(1.0, maximum(Φ_r[tmp] ./ Φ[tmp])) .+  Dh*0
                    )
        pg =  SUGSET.make_stack(t, typeof(pg), pg)
    
    return pg
end
