    using PyPlot
# using VAWTools
# using JLD
# using StatsBase
ENV["PYTHON"]=""
using Parameters
using DifferentialEquations
import Roots: find_zero
using StatsBase
#using Dates
using SUGSET: erosion, post_proc
import SUGSET:  zs,zb, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,Num,startup, Phys
using SUGSET

if homedir()== "/Users/Ian"
    fp_load = "/Users/Ian/research/SUGSET-2D/model_outputs/"
    fp_save = "/Users/Ian/research/SUGSET-2D/figs/"
end

run_type="no_ero"
include("interface_alpine.jl")


pp=Phys()
       
pn = Num()


pg= Valley(tspan = (90*day, 270*day),
           )

pg= Valley(pg,
           tout=(pg.tout[1]:0.25*day:pg.tout[end])
           )
pg,pp,pn = startup(pg, Valley, pp,pn)

tout=pg.tout
error()
#########################
function make_routing_plot(pg,pp,pn)
    tout=pg.tout
    Qw_catch= []#zeros(length(pg.tout))
    
    # for i = 1:length(pg.tout)
    #     t= tout[i]
    #     _,Qw,_,pg_here= SUGSET.Qw_return(t,pg,pp,pn)
    #     push!(Qw_catch, -sum(Qw[pg_here.edges]))
    # end

    
    fs=12
    fig1 = plt.figure(figsize=(10,6))
    Threads.@threads  for i = 1:length(pg.tout)
        t=tout[i]
        println(t./day)
        _,Qw,S, pg_ff= SUGSET.Qw_return(t,pg,pp,pn)
        push!(Qw_catch, -sum(Qw[pg_ff.edges]))
        
        ######
        plt.subplot2grid((14,2), (0,0),rowspan=2, colspan=2)
        p1=plt.plot(tout[1:i]./year, Qw_catch[1:i],linewidth="2", color="steelblue", label = "Water discharge" )
        ylim([0, 17])
        #plt.xticks([])
        plt.xlim([pg.tout[1]./year, pg.tout[end]./year ])
        xlabel("Year", fontsize=fs)
        plt.ylabel(L"Q_{w}~(m³ s⁻¹)",color="steelblue", fontsize=fs)
        plt.title("Day  $(round(t/day,digits=2))",fontsize=fs*1.2)
        plt.axvline(tout[i]./year,linewidth="3",color="black")
        xlabel("x (m)")
        ylabel("y (m)")

        #######
        plt.subplot2grid((14,2), (6,0),rowspan=2, colspan=1)
        im1=imshow(reshape(SUGSET.vectorize_output(pg,pp,pn, t, thick), pg.nh,pg.ns),extent=[minimum(pg.sv),maximum(pg.sv),minimum(pg.hv),maximum(pg.hv)]);
        colorbar(im1,shrink=.65,pad=.05,aspect=10)
        #ax3.set_label("Sediment discharge (m³ s⁻¹)")
        plt.title("Glacier thickness (m)", fontsize=fs)
        xlabel("x (m)")
        ylabel("y (m)")

        #########
        plt.subplot2grid((14,2), (6,1),rowspan=2, colspan=1)
        im2=imshow(reshape(abs.(Qw./S), pg.nh,pg.ns),extent=[minimum(pg.sv),maximum(pg.sv),minimum(pg.hv),maximum(pg.hv)]);colorbar(im2,shrink=.65,pad=.05, aspect=10)
        clim([0,1])
        title("Velocity (m s⁻¹)", fontsize=fs)
        xlabel("x (m)")
        ylabel("y (m)")

        plt.subplot2grid((14,2), (11,0),rowspan=2, colspan=1)
        im3=imshow(reshape(-Qw, pg.nh,pg.ns),extent=[minimum(pg.sv),maximum(pg.sv),minimum(pg.hv),maximum(pg.hv)]);colorbar(im3,shrink=.65,pad=.05,aspect=10);clim([0.4, 6])
        title("Water discharge (m³ s⁻¹)", fontsize=fs)
        xlabel("x (m)")
        ylabel("y (m)")
        
        plt.subplot2grid((14,2), (11,1),rowspan=2, colspan=1)
        im4=imshow(reshape(S, pg.nh,pg.ns),extent=[minimum(pg.sv),maximum(pg.sv),minimum(pg.hv),maximum(pg.hv)]);colorbar(im4,shrink=.65,pad=.05,aspect=10);clim([0, 9])
        title("Channel cross-section, S (m²)", fontsize=fs)
        xlabel("x (m)")
        ylabel("y (m)")

        subplots_adjust(hspace=-0.6)
        tight_layout()
        #pause(0.1)
        savefig("/Users/Ian/research/SUGSET-2D/model_outputs/video/routing_video/routing_video_alpine$(i+1000)_original.png")
        clf()
        
    end
end
make_routing_plot(pg,pp,pn)

error()
#savefig("$(fp_save)illustration.pdf")
using FileIO, ProgressMeter, Images
imgnames = filter(x->occursin(".png",x),readdir("/Users/Ian/research/SUGSET-2D/model_outputs/video/routing_video/")) # Populate list of all .pngs
intstrings_ =  map(x->split(x,".")[1],imgnames) # Extract index from filenames
intstrings =  map(x->split(x,"_")[3],intstrings_) # Extract index from filenames
p = sortperm(parse.(Int,intstrings)) #sort files numerically
imgstack = []
@showprogress for i=1:length(imgnames) #in imgnames[p]
    #println(imgnames[i])
    push!(imgstack,RGB.(load("/Users/Ian/research/SUGSET-2D/model_outputs/video/routing_video/$(imgnames[i])")))
end

using VideoIO
props = [:priv_data => ("crf"=>"0","preset"=>"ultrafast")]
encodevideo("/Users/Ian/research/SUGSET-2D/model_outputs/video/routing_video/routing_video.mp4",imgstack,   framerate=4,AVCodecContextProperties=props)


